import org.scalatest.{FunSpec, GivenWhenThen}

class ChapterEightTest extends FunSpec with GivenWhenThen {
  describe("Task 1") {
    describe("when BankAccount") {
      it("should not add comission to deposit") {
        Given("empty bank account")
        val eight: ChapterEight = new ChapterEight()
        val bankAccount: ChapterEight#BankAccount = new eight.BankAccount(0.0)

        When("deposit 10$")
        bankAccount.deposit(10.0)

        Then("should have 10$")
        assert(10.0 == bankAccount.balance)
      }

      it("should not add commision to withdraw") {
        Given("bank account with 10$")
        val eight: ChapterEight = new ChapterEight()
        val bankAccount: ChapterEight#BankAccount = new eight.BankAccount(10.0)

        When("withdraw 10$")
        val withdrawed: Double = bankAccount.withdraw(10.0)

        Then("balance empty")
        assert(0.0 == bankAccount.balance)
      }
    }

    describe("when CheckingAccount") {
      it("should add comission 1$ to deposit") {
        Given("empty bank account")
        val eight: ChapterEight = new ChapterEight()
        val bankAccount: ChapterEight#BankAccount = new eight.CheckingAccount(0.0)

        When("deposit 10$")
        bankAccount.deposit(10.0)

        Then("should have 10$")
        assert(9.0 == bankAccount.balance)
      }

      it("should add commision to withdraw") {
        Given("bank account with 11$")
        val eight: ChapterEight = new ChapterEight()
        val bankAccount: ChapterEight#BankAccount = new eight.CheckingAccount(11.0)

        When("withdraw 10$")
        val withdrawed: Double = bankAccount.withdraw(10.0)

        Then("balance empty")
        assert(0.0 == bankAccount.balance)
      }
    }
  }

  describe("Task 2") {
    describe("SavingsAccount") {
      it("should allow 3 deposits without commision") {
        Given("")
        val eight: ChapterEight = new ChapterEight()
        val bankAccount: ChapterEight#BankAccount = new eight.SavingsAccount(0.0)

        When("")
        bankAccount.deposit(10.0)
        bankAccount.deposit(10.0)
        bankAccount.deposit(10.0)

        Then("")
        assert(30.0 == bankAccount.balance)
      }

      it("should take comission after 3 deposits") {
        Given("")
        val eight: ChapterEight = new ChapterEight()
        val bankAccount: ChapterEight#BankAccount = new eight.SavingsAccount(0.0)
        bankAccount.deposit(10.0)
        bankAccount.deposit(10.0)
        bankAccount.deposit(10.0)

        When("")
        bankAccount.deposit(10.0)

        Then("")
        assert(39.0 == bankAccount.balance)
      }

      it("should allow 3 withdraw without commision") {
        Given("")
        val eight: ChapterEight = new ChapterEight()
        val bankAccount: ChapterEight#BankAccount = new eight.SavingsAccount(30.0)

        When("")
        bankAccount.withdraw(10.0)
        bankAccount.withdraw(10.0)
        bankAccount.withdraw(10.0)

        Then("")
        assert(0.0 == bankAccount.balance)
      }

      it("should take comission after 3 withdrowal") {
        Given("")
        val eight: ChapterEight = new ChapterEight()
        val bankAccount: ChapterEight#BankAccount = new eight.SavingsAccount(50.0)
        bankAccount.withdraw(10.0)
        bankAccount.withdraw(10.0)
        bankAccount.withdraw(10.0)

        When("")
        bankAccount.withdraw(10.0)

        Then("")
        assert(9.0 == bankAccount.balance)
      }
    }
  }
}
