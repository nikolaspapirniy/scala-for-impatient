import org.scalatest.FlatSpec

class ChapterFiveTest extends FlatSpec {

  trait Before {
    val five: ChapterFive = new ChapterFive
    val one: ChapterFive#One = new five.One
  }

  "Task one" should "change value to 0 if maxint" in new Before {
    // when
    one.value = Int.MaxValue

    // then
    assert(0 === one.value)
  }

  it should "leave value the same if not maxint" in new Before {
    // given
    val notMaxInt: Int = 10

    // when
    one.value = notMaxInt

    // then
    assert(notMaxInt === one.value)
  }

  "Task 4" should "verify that date before another date" in new Before {
    // given
    val dateOne = new five.Time(10, 30)
    val dateTwo = new five.Time(10, 31)

    // then
    assert(dateOne.before(dateTwo))
  }

  it should "verify that date after another date" in new Before {
    // given
    val dateOne = new five.Time(10, 32)
    val dateTwo = new five.Time(10, 31)

    // then
    assert(!dateOne.before(dateTwo))
  }

  it should "return false if two dates are equals" in new Before {
    // given
    val dateOne = new five.Time(10, 32)

    // then
    assert(!dateOne.before(dateOne))
  }

  it should "throw exception if hours < 0" in new Before {
    intercept[IllegalArgumentException] {
      new five.Time(-10, 32)
    }
  }

  it should "throw exception if hours > 24" in new Before {
    intercept[IllegalArgumentException] {
      new five.Time(25, 32)
    }
  }

  it should "throw exception if hours = 24" in new Before {
    intercept[IllegalArgumentException] {
      new five.Time(24, 32)
    }
  }

  it should "throw exception if minutes < 0" in new Before {
    intercept[IllegalArgumentException] {
      new five.Time(20, -5)
    }
  }

  it should "throw exception if minutes > 60" in new Before {
    intercept[IllegalArgumentException] {
      new five.Time(20, 65)
    }
  }

  it should "throw exception if minutes = 60" in new Before {
    intercept[IllegalArgumentException] {
      new five.Time(20, 60)
    }
  }

  "Task 6" should "replace negative value with 0" in new Before {
    // when
    val person: ChapterFive#Person = new five.Person(-1)

    // then
    assert(person.age == 0)
  }

  it should "replace not replace non negative value" in new Before {
    // given
    val aboveZero = 10

    // when
    val person: ChapterFive#Person = new five.Person(aboveZero)

    // then
    assert(person.age == aboveZero)
  }
}
