import org.scalatest._
import org.scalatest.FreeSpec

abstract class UnitSpec extends FreeSpec with Matchers with
OptionValues with Inside with Inspectors with GivenWhenThen
