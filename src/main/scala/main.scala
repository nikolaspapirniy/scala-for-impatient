import scala.collection.mutable.ArrayBuffer

object main {
  def main(args: Array[String]) {
    val ints: Array[Int] = Array[Int](1, 2, 3, 4, 5)

    var arr = ArrayBuffer[Int]()
    val r = for (i <- 1 until ints.length by 2) yield ints(i)

    var iterator: Int = 0

    println("r = " + r.mkString(" "))

    for (i <- 0 until ints.length by 2) {
      if (iterator != r.length) {
        println(r(iterator))
        iterator += 1
      }
      println(ints(i))
    }
    //r.copyToArray(arr)
    //r.copyToArray(arr)
    /*
        var iter = 0

        println("arr.length = " + arr.length)

        for (i <- 0 until ints.length by 2) {
          if(iter != arr.length) {
            println("arr(iter) = " + arr(iter))
            iter += 1
          }
          println("ints = " + ints(i))
        }*/
  }
}

