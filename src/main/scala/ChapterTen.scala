

object ChapterTen extends App {

  trait Logger {
    val filename = "Logger"
    println("Logger constructor")

    def log(msg: String) {
      println("logger" + filename)
    }
  }

  trait ConsoleLogger extends Logger {
    println("ConsoleLogger constructor")

    override def log(msg: String) = {
      println("ConsoleLogger")
      super.log(msg)
    }
  }

  trait ShortLogger extends Logger {
    println("ShortLogger constructor")

    override def log(msg: String) = {
      println("ShortLogger")
      super.log(msg)
    }
  }

  trait LoggedException extends Logger {
    this: Exception =>
    override def log(msg: String): Unit = println("Logged")
  }

  private val exception: Throwable with LoggedException = new Throwable with LoggedException
  exception.log("Hasdasd")

}
