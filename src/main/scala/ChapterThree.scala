import java.util.TimeZone

import scala.collection.immutable
import scala.collection.mutable._

object ChapterThree {
  def main(args: Array[String]) {
    new ChapterThree().nine()
  }
}

class ChapterThree {
  def nine() = {
    val availableTimeZones: Array[String] = TimeZone.getAvailableIDs
    val filtered: Array[String] = availableTimeZones.filter(!_.startsWith("America/"))

    println(filtered.mkString(" "))
  }

  def eight() = {
    val a: ArrayBuffer[Int] = ArrayBuffer[Int](-1, 2, 3, -5, -10, 100)

    val indexes = for (i <- 0 until a.length if a(i) < 0) yield i
    val negative: immutable.IndexedSeq[Int] = indexes.drop(1).reverse

    for (j <- negative) a.remove(j)
    println(a.mkString(" "))
  }

  def seven(ints: Array[Int]): Array[Int] = {
    val sorted: Array[Int] = ints.sortWith(_ < _)

    if (sorted.length <= 1) {
      sorted
    } else {
      val result: ArrayBuffer[Int] = ArrayBuffer[Int]()
      for (i <- 1 until sorted.length) {
        def isLastElement: Boolean = {
          i == sorted.length - 1
        }
        if (isLastElement) {
          if (sorted(i - 1) != sorted(i))
            result += sorted(i)
        } else {
          if (sorted(i - 1) != sorted(i) && sorted(i) != sorted(i + 1))
            result += sorted(i)
        }
      }
      result.toArray
    }
  }

  def seven_groupedBy(ints: Array[Int]): Array[Int] = {
    val groupedBy: immutable.Map[Int, Array[Int]] = ints.groupBy(w => w)
    val result: ArrayBuffer[Int] = ArrayBuffer[Int]()

    for (kv <- groupedBy) {
      if (kv._2.size < 2)
        result += kv._1
    }
    result.toArray.sortWith(_ < _)
  }


  def six(ints: Array[Int]): Array[Int] = {
    ints.sortWith(_ < _).reverse
  }

  def six(ints: ArrayBuffer[Int]): ArrayBuffer[Int] = {
    ints.sorted.reverse
  }

  private def swap(arr: ArrayBuffer[Int], x: Int, y: Int) = {
    val tmp = arr(x)
    arr(x) = arr(y)
    arr(y) = tmp
  }

  def two(arr: Array[Int]): Array[Int] = {
    val result: ArrayBuffer[Int] = ArrayBuffer(arr: _*)

    for (i <- (0 until arr.length by 2)) {
      if (i != arr.length - 1) {
        swap(result, i, i + 1)
      }
    }
    result.toArray[Int]
  }

  def three(arr: Array[Int]): Array[Int] = {
    var result = ArrayBuffer[Int]()
    val r = for (i <- 1 until arr.length by 2) yield arr(i)

    var iterator: Int = 0

    for (i <- 0 until arr.length by 2) {
      if (iterator != r.length) {
        result += r(iterator)
        iterator += 1
      }
      result += arr(i)
    }
    result.toArray
  }

  def four(ints: Array[Int]): Array[Int] = {
    val positive: Array[Int] = ints.filter(p => p > 0)
    val negative: Array[Int] = ints.filter(p => p <= 0)
    positive ++ negative
  }

  def five(doubles: Array[Double]): Double = {
    if (doubles.isEmpty)
      0.0
    else
      doubles.sum / doubles.length
  }

  def five_recursive(doubles: Array[Double]): Double = {
    def sum(doubles: Array[Double]): Double = {
      if (doubles.isEmpty) 0.0 else doubles.head + sum(doubles.tail)
    }
    if (doubles.isEmpty) 0.0 else sum(doubles) / doubles.length
  }
}
