import scala.beans.BeanProperty

class ChapterFive {

  class One {
    private var privateValue = 0

    case class Person(name: String)

    def value_=(newValue: Int): Unit = {
      if (newValue == Int.MaxValue) privateValue = 0 else privateValue = newValue
    }

    def value = privateValue
  }

  class BankAccount {
    private var privBalance: Int = 0

    def balance = privBalance
  }

  class Time(private val _hrs: Int, private val _minutes: Int) {
    require(hrs >= 0 && hrs < 24)
    require(minutes >= 0 && minutes < 60)

    private val countInMinutes: Int = hrs * 60 + minutes

    def hrs: Int = _hrs

    def minutes: Int = _minutes

    def before(other: Time): Boolean = countInMinutes < other.countInMinutes
  }

  class Student {
    @BeanProperty
    var name: String = _

    @BeanProperty
    var id: Long = _
  }

  class Person(private val _age: Int) {
    def age: Int = if (_age < 0) 0 else _age
  }

  class Person2(val name: String) {

    @throws(classOf[IllegalArgumentException])
    def this() {
      this("nasdasd")
    }
  }
}

object ChapterFive {
  def main(args: Array[String]): Unit = {
    val five: ChapterFive = new ChapterFive
    val one: ChapterFive#One = new five.One
    val bank: ChapterFive#BankAccount = new five.BankAccount
    val time: ChapterFive#Time = new five.Time(10, 32)
    val student: ChapterFive#Student = new five.Student

    println(student.getName)
    println(student.getId)
  }
}
