class ChapterEight {

  class BankAccount(private val initialBalance: Double) {
    private var _balance = initialBalance

    def deposit(amount: Double) = {
      _balance += amount;
      _balance
    }

    def withdraw(amount: Double) = {
      _balance -= amount;
      _balance
    }

    def balance = _balance
  }

  class CheckingAccount(initialBalance: Double) extends BankAccount(initialBalance) {
    override def deposit(amount: Double): Double = super.deposit(amount - 1)

    override def withdraw(amount: Double): Double = super.withdraw(amount + 1)
  }

  class SavingsAccount(private val initialBalance: Double) extends BankAccount(initialBalance) {
    var countOfOperationsThisMonth = 0

    override def deposit(amount: Double): Double = {
      countOfOperationsThisMonth += 1
      if (countOfOperationsThisMonth > 3) super.deposit(amount - 1.0) else super.deposit(amount)
    }

    override def withdraw(amount: Double): Double = {
      countOfOperationsThisMonth += 1
      if (countOfOperationsThisMonth > 3) super.withdraw(amount + 1.0) else super.withdraw(amount)
    }
  }

  abstract class Item {
    def price: Int

    def description: String
  }

  class SimpleItem protected(val price: Int, val description: String) extends Item {
  }

  class Creature {
    val range: Int = 10
    val env: Array[Int] = new Array[Int](range)
  }

  class Ant extends Creature {
    override val range = 2
  }

  class Bug extends {
    override val range = 3
  } with Creature

}

object ChapterEight extends App {
  private val eight: ChapterEight = new ChapterEight
  private val ant: ChapterEight#Ant = new eight.Ant
  private val bug: ChapterEight#Bug = new eight.Bug
  println("ant = " + ant.env.length)
  println("bug = " + bug.env.length)
}
